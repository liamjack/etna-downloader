<?php

class EtnaApiClient
{
    private $username;
    private $password;

    private $cookieJar;

    private $guzzle;

    public function __construct($username, $password, $timeout = 60) {
        $this->username = $username;
        $this->password = $password;

        $this->cookieJar = new \GuzzleHttp\Cookie\CookieJar();

        $this->guzzle = new \GuzzleHttp\Client([
            'timeout' => $timeout,
            "headers" => [
                "User-Agent" => "Mozilla/5.0 (Windows NT 10.0; rv:68.0) Gecko/20100101 Firefox/68.0"
            ],
            "cookies" => $this->cookieJar,
            "allow_redirects" => true
        ]);
    }

    public function login() {
        $response = $this->guzzle->request("POST", "https://auth.etna-alternance.net/identity", [
            'json' => [
                'login' => $this->username,
                'password' => $this->password
            ]
        ]);

        if ($response->getStatusCode() != 200)
            throw new Exception("Login failed.");
    }

    public function getPromotions() {
        $response = $this->guzzle->get("https://intra-api.etna-alternance.net/promo");

        return json_decode($response->getBody());
    }

    public function getPromotionModules($promotionId) {
        $response = $this->guzzle->get("https://modules-api.etna-alternance.net/students/{$this->username}/search?role=students&term_id={$promotionId}");

        return json_decode($response->getBody());
    }

    public function getModuleActivities($moduleId) {
        $response = $this->guzzle->get("https://modules-api.etna-alternance.net/{$moduleId}/activities");

        return json_decode($response->getBody());
    }

    public function getActivityFiles($moduleId, $activityId) {
        $response = $this->guzzle->get("https://modules-api.etna-alternance.net/{$moduleId}/activities/{$activityId}/files");

        return json_decode($response->getBody());
    }

    public function downloadFile($path, $filePath) {
        $response = $this->guzzle->get("https://modules-api.etna-alternance.net" . $path, [
            'sink' => $filePath
        ]);
    }
}