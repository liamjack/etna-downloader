<?php

require_once("vendor/autoload.php");

require_once("EtnaApiClient.php");

$username = "dupon_j";
$password = "password";
$rootDir = "/tmp/etna-downloader";

$etnaApiClient = new EtnaApiClient($username, $password);

$etnaApiClient->login();
$promotions = $etnaApiClient->getPromotions();

echo "[INFO] " . count($promotions) . " promos found.\n";

foreach ($promotions as $promotion) {
    echo "[INFO] Promo #{$promotion->id} - {$promotion->wall_name}\n";

    $promotionPath = "{$rootDir}/{$promotion->wall_name}";

    if (!file_exists($promotionPath))
        if (!mkdir($promotionPath))
            throw new Exception("Error encountered creating promotion directory.");

    $modules = $etnaApiClient->getPromotionModules($promotion->id);

    echo "[INFO] " . count($modules) . " modules found.\n";
    
    foreach ($modules as $module) {
        echo "[INFO] Module #{$module->id} - {$module->name}\n";

        $modulePath = "{$promotionPath}/{$module->name}";

        if (!file_exists($modulePath))
            if (!mkdir($modulePath))
                throw new Exception("Error encountered creating module directory.");

        $activities = $etnaApiClient->getModuleActivities($module->id);

        echo "[INFO] " . count($activities) . " activities found.\n";

        foreach ($activities as $activity) {
            echo "[INFO] Activity #{$activity->id} - {$activity->name}\n";

            $activityPath = "{$modulePath}/{$activity->name}";

            if (!file_exists($activityPath))
                if (!mkdir($activityPath))
                    throw new Exception("Error encountered creating activity directory.");

            $files = $etnaApiClient->getActivityFiles($module->id, $activity->id);

            echo "[INFO] " . count($files) . " files found.\n";

            foreach ($files as $file) {
                echo "[INFO] File - {$file->filename}\n";

                $filePath = "{$activityPath}/{$file->filename}";

                if (file_exists($filePath)) {
                    echo "[INFO] File already exists, skipping.";
                    continue;
                }

                $etnaApiClient->downloadFile($file->path, $filePath);
            }
        }
    }
}